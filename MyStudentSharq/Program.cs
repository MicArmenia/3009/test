﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyStudentSharq
{
    class Program
    {
         static Random rnd = new Random();
        static void Main(string[] args)
        {
            
            List<Student> students = CreateStudent(11);
            // Shuffle(students);
            // PrintStudents(students);
            var group = GropStudets(students, 5);
            PrintStudentsByGroup(group);
            Console.Read();
        }


        static void PrintStudentsByGroup(List<List<Student>> source)
        {
            for (int i = 0; i < source.Count; i++)
            {
                Console.WriteLine($"--------{i + 1}sharq -------");
                Console.WriteLine();
                for (int j = 0; j < source[i].Count; j++)
                {
                    
                    Console.WriteLine(source[i][j].Name);
                    Console.WriteLine(source[i][j].Surname);
                    Console.WriteLine(source[i][j].Email);
                    Console.WriteLine();
                }
            }

        }
        static List<List<Student>> GropStudets(List<Student> source,int count)
        {
            var group = new List<List<Student>>();
            int totalCount = count;

            for (int i = 0; i < count; i++)
            {
                int curentCount = source.Count / totalCount;

                List<Student> st = CutFromTop(source, curentCount);
                group.Add(st);
                totalCount--;
            }

            return group;
        }

        static List<Student> CutFromTop(List<Student>source,int count)
        {
            var items = new List<Student>(count);
            while (count>0)
            {
                Student st = source[0];
                items.Add(st);
                source.RemoveAt(0);
                count--;
            }

            return items;
        }
        static void PrintStudents(List<Student> source)
        {
            foreach (var item in source)
            {
                Console.WriteLine(item.Name);
                Console.WriteLine(item.Surname);
                Console.WriteLine(item.Email);
                Console.WriteLine();
            }
        }
        static List<Student> CreateStudent(int count)
        {
            List<Student> source = new List<Student>();
            for (int i = 0; i < count; i++)
            {
                Student st = new Student();
                st.Name = $"A{i+1}";
                st.Surname = $"A{i+1}yan";
                st.Email = $"A{i+1}@gmail.com";

                source.Add(st);
            }

            return source;
        }
        
        static void Shuffle(List<Student> source)
        {
           
            for (int i = 0; i < source.Count; i++)
            {
                int index = rnd.Next(i, source.Count);
                Swap(source, i, index);
            }
        }
        static void Swap(List<Student> source, int i, int index)
        {
            Student temp = source[i];
            source[i] = source[index];
            source[index] = source[i];
        }
    }
}
